#================================================================================
#       Program to make aqcusition parameter files that are gridded
#
#       Usage:
#               >> python make_grid.py par=gridparamfile
#
#       Where the gridparamfile is in the format (fill in the xxxx parts):
#
#               [grid_parameters]
#               NXlines= xxxx
#               OXlines= xxxx
#               DXlines= xxxx
#               NYlines= xxxx
#               OYlines= xxxx
#               DYlines= xxxx
#               depth= xxxx
#
#
#       Taylor Dahlke, 11/22/2017 taylor@sep.stanford.edu
#================================================================================



#!/usr/bin/python

import subprocess
import sepbasic
import sys

# Read in the input parameters
eq_args_from_cmdline, args = sepbasic.parse_args(sys.argv)
pars=sepbasic.RetrieveAllEqArgs(eq_args_from_cmdline)
NXlines=int(pars["NXlines"])
NYlines=int(pars["NYlines"])
OXlines=float(pars["OXlines"])
OYlines=float(pars["OYlines"])
DXlines=float(pars["DXlines"])
DYlines=float(pars["DYlines"])
depth=int(pars["depth"])
outfile=pars["outfile"]

# Make a list of all the files for each job ( shots per job)
for ix in range(NXlines):
	x = OXlines + ix*DXlines
	for iy in range(NYlines):
		y = OYlines + iy*DYlines
		cmd = "echo %s %s %s >> %s" % (depth,x,y,outfile)
		subprocess.call(cmd,shell=True)






		