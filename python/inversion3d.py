from modeled_data import *
from born import *
import subprocess
import os.path
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math


class inversion(object):
    def __init__(self, param_reader,prefix):
        #----------------------------------------------------------------
        self.dict_args = param_reader.dict_args
        self.res_path = self.dict_args['res_path']
        self.obs_path = self.dict_args['obs_path']
        self.syn_path = self.dict_args['syn_path']
        self.rtm_path = self.dict_args['rtm_path']
        self.vel_path = self.dict_args['vel_path']
        self.pathtemphess = self.dict_args['pathtemphess']
        self.gensolve_F = "%s/forward_hessPBS.txt" % (self.pathtemphess)
        self.gensolve_A = "%s/adjoint_hessPBS.txt" % (self.pathtemphess)
        self.solverpath = self.dict_args['solverpath']
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nfiles = int(self.dict_args['nfiles'])  # Same as number of shots
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.genpar = self.dict_args['genpar']
        self.pbs_template = self.dict_args['pbs_template']
        self.prefix = prefix
        self.nfiles_perjob = int(self.dict_args['nfiles_perjob'])
        self.hess_iter = self.dict_args['hess_iter']
        self.fullsrcwave_name = self.dict_args['fullsrcwave_name']
        self.fullrecwave_name = self.dict_args['fullrecwave_name']
        self.rbf_path = self.dict_args['rbf_path']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.masksalt = self.dict_args['masksalt']
        self.maskvelb = self.dict_args['maskvelb']
        self.scaling = self.dict_args['scaling']
        self.phigrad = self.dict_args['phigrad']
        self.tomograd = self.dict_args['tomograd']
        self.rbftable = self.dict_args['rbftable']
        self.queues = self.dict_args['queues']
        self.queues_cap = self.dict_args['queues_cap']
        self.pypath = self.dict_args['pypath']
        # self.outputfilename = self.dict_args['outputfilename']
        return



    def hessGN_RBF1(self, debug):
        text_fileA = open(self.gensolve_F, "w")
        text_fileF = open(self.gensolve_A, "w")
        args1f = "pbs_template=%s genpar=%s vel_path=%s path_out=%s T_BIN=%s wavelet=%s" % (
            self.pbs_template, self.genpar, self.vel_path, self.path_out, self.T_BIN, self.wavelet)
        args2f = "fullrecwave_name=%s fullsrcwave_name=%s res_path=%s" % (
            self.fullrecwave_name, self.fullsrcwave_name, self.res_path)
        args3f = "nfiles_perjob=%s prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        args4f = "hess_in_path=input.H hess_out_path=output.H"
        args5f = "rbftable=%s maskvelb=%s masksalt=%s scaling=%s rbf_path=%s rbfcoord=%s" % (
            self.rbftable, self.maskvelb, self.masksalt, self.scaling, self.rbf_path, self.rbfcoord)
        cmdF = "RUN: python %s/hessian_GN_RBF1.py %s %s %s %s %s >> hessian_GN_RBF1.log" % (
            self.pypath, args1f, args2f, args3f, args4f, args5f)
        text_fileA.write("%s" % cmdF)
        text_fileA.close()
        text_fileF.write("%s" % cmdF)
        text_fileF.close()
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
        return



    def hessGN_PLAIN(self, debug):
        text_fileA = open(self.gensolve_F, "w")
        text_fileF = open(self.gensolve_A, "w")
        args1f = "pbs_template=%s genpar=%s vel_path=%s path_out=%s T_BIN=%s wavelet=%s" % (
            self.pbs_template, self.genpar, self.vel_path, self.path_out, self.T_BIN, self.wavelet)
        args2f = "fullrecwave_name=%s fullsrcwave_name=%s res_path=%s" % (
            self.fullrecwave_name, self.fullsrcwave_name, self.res_path)
        args3f = "nfiles_perjob=%s prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        args4f = "hess_in_path=input.H hess_out_path=output.H"
        args5f = "maskvelb=%s masksalt=%s scaling=%s" % (
            self.maskvelb, self.masksalt, self.scaling)
        cmdF = "RUN: python %s/hessian_GN_PLAIN.py %s %s %s %s %s >> hessian_GN_PLAIN.log" % (
            self.pypath, args1f, args2f, args3f, args4f, args5f)
        text_fileA.write("%s" % cmdF)
        text_fileA.close()
        text_fileF.write("%s" % cmdF)
        text_fileF.close()
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
        return


    def hessGN_PLAINmod(self, debug):
        text_fileA = open(self.gensolve_F, "w")
        text_fileF = open(self.gensolve_A, "w")
        args1f = "pbs_template=%s genpar=%s vel_path=%s path_out=%s T_BIN=%s wavelet=%s" % (
            self.pbs_template, self.genpar, self.vel_path, self.path_out, self.T_BIN, self.wavelet)
        args2f = "fullrecwave_name=%s fullsrcwave_name=%s res_path=%s" % (
            self.fullrecwave_name, self.fullsrcwave_name, self.res_path)
        args3f = "nfiles_perjob=%s prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        args4f = "hess_in_path=input.H hess_out_path=output.H"
        args5f = "maskvelb=%s masksalt=%s scaling=%s" % (
            self.maskvelb, self.masksalt, self.scaling)
        cmdF = "RUN: python %s/hessian_GN_PLAINmod.py %s %s %s %s %s >> hessian_GN_PLAIN.log" % (
            self.pypath, args1f, args2f, args3f, args4f, args5f)
        text_fileA.write("%s" % cmdF)
        text_fileA.close()
        text_fileF.write("%s" % cmdF)
        text_fileF.close()
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
        return


    def hessMAIN(self, param_reader, debug, GNhess, RBF, phiANDvelb, gradtag, symmetric_in, inputmodel, hess_out_path):
        #-------------------------------------------------------------------------------------
        # Write the FORWARD and ADJOINT .txt files for the generic solver
        if (GNhess):
            if (RBF):
                self.hessGN_RBF1(debug)
            else:
                # self.hessGN_PLAIN(debug)
                self.hessGN_PLAINmod(debug)
        else:
            if (phiANDvelb):
                self.hessFULL_RBF2(debug, gradtag)
            else:
                self.hessFULL_RBF1(debug)

        # Make input data
        cmd = "Cp %s %s/inputdata.H" % (inputmodel, self.pathtemphess)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making input data"
            print(cmd)
        subprocess.call(cmd, shell=True)

        # Make a zero initial model
        cmd = "Cp %s/inputdata.H %s/initmodel.H; Solver_ops file1=%s/initmodel.H op=zero;" % (
            self.pathtemphess, self.pathtemphess, self.pathtemphess)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making a zero initial model"
            print(cmd)
        subprocess.call(cmd, shell=True)

        if (symmetric_in):
            symmetric = 'yes'
        else:
            symmetric = 'no'

        if(not os.path.isfile(hess_out_path)):
            # CG SOLVE
            cmd1 = "python %s/python_solver/generic_linear_prob.py  fwd_cmd_file=%s  adj_cmd_file=%s iteration_movies=obj,model,gradient,residual " % (
                self.solverpath, self.gensolve_F, self.gensolve_A)
            cmd2 = "debug=no data=%s/inputdata.H init_model=%s/initmodel.H  niter=%s  inv_model=%s suffix=%s symmetric=%s  dotprod=0" % (
                self.pathtemphess, self.pathtemphess, self.hess_iter, hess_out_path, self.prefix, symmetric)
            cmd = "%s %s" % (cmd1, cmd2)  # tolobjrel=0.05
            if (debug):
                print(
                    '-----------------------------------------------------------------')
                print " LAUNCHING GENERIC PYTHON SOLVER FOR HESSIAN"
                print(cmd)
            subprocess.call(cmd, shell=True)
        else:
            print "------------------------------------------------------------"
            print "Inverted result already exists: Skipping Hessian Inversion"

        return


        return
