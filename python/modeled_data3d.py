#!/usr/local/bin/python

import time
import pbs_util
import subprocess
import math, copy
from batch_task_executor import *


class data_modeling(BatchTaskComposer):
  def __init__(self, param_reader, vel_path, prefix):
      BatchTaskComposer.__init__(self)

      #Read in the job parameters
      self.dict_args = param_reader.dict_args
      self.T_BIN = self.dict_args['T_BIN']
      self.wavelet = self.dict_args['wavelet']
      self.vel_path = vel_path
      self.nshots = int(self.dict_args['nfiles'])
      self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
      self.ish_beg = int(self.dict_args['ish_beg'])
      self.path_out = param_reader.path_out
      self.prefix = prefix
      self.pathtemp = self.dict_args['pathtemp']
      self.genpar = self.dict_args['genpar']
      self.writesrcwave = int(self.dict_args['writesrcwave'])
      self.fullsrcwave_name = self.dict_args['fullsrcwave_name']

      # Create all subjobs info
      self.njobs = int(math.ceil(self.nshots/float(self.nsh_perjob)))
      self.subjobids = range(0, self.njobs)
      self.subjobfns_list = [None]*self.njobs
      self.subjobIND_list = range(self.ish_beg, self.nshots+1, self.nsh_perjob) # List of actual shot number (starting @ 0)

      # Make a list of all the files for each job ( shots per job)
      for i in range(0,self.njobs):
          ish_start = self.subjobIND_list[i]
          nsh = min(self.nsh_perjob, self.nshots-ish_start)
          self.subjobfns_list[i] = [None]*nsh
          for j in range(0,nsh):
              self.subjobfns_list[i][j] = '%s/%s-%s.H' % (self.path_out,self.prefix,ish_start+j+1)
      return


  def GetSubjobScripts(self, subjobid):
      #'Return the scripts content for that subjob.'
      subjobfns = self.subjobfns_list[subjobid]
      # Build scripts that create the files designated in subjobfns
      scripts = []
      cnt = 0
      nf_act = len(subjobfns) # number of files
      for fn in subjobfns:
          shot = self.nsh_perjob*subjobid+cnt+1
          cnt += 1
          if cnt > nf_act: break
          operator = "%s/%s" % (self.T_BIN,'FBI.x')
          inputs = 'isou=%d adj=0 oper=0 gpu_n=1 gpu_o=1 < %s wavelet=%s par=%s' % (shot,self.vel_path,self.wavelet,self.genpar)
          outputs = '> %s' % (fn)
          srcwaveout = ''
          compression = ''
          if (self.writesrcwave):
            srcwaveout = 'src_wave=%s/%s_%s.H' % (self.path_out,self.fullsrcwave_name,shot)
            # srcwaveout = 'src_wave=%s/src_wave%s_fullo.H' % (self.path_out,shot)
          cmd = '%s %s %s %s ; %s\n' % (operator, inputs, outputs, srcwaveout, compression)
          # Note: need the \n at the end... also cant use < in string itself.
          scripts.append(cmd)
      return scripts, str(subjobid)




  def GetSubjobsInfo(self):
      '''Should return two lists, subjobids and subjobfns.'''
      return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)




class make_MODELED_DATA(object):
  def __init__(self, param_reader):
      dict_args = param_reader.dict_args
      self.file_prefix = dict_args['file_prefix']
      self.vel_path = dict_args['vel_path']
      self.outputpath = dict_args['outputpath']
      return



  def MODELED_DATA(self, param_reader, debug):
      #----- MAKE "MODELED" DATA (since we dont have real field data) -------------------
      modeled_data = data_modeling(param_reader,self.vel_path,self.file_prefix)
      bte = BatchTaskExecutor(param_reader)
      prefix = param_reader.prefix
      if (debug):
          print('-----------------------------------------------------------------')
          print(' MAKING MODELED DATA')
      bte.LaunchBatchTask(prefix, modeled_data)
      # Combine the files to one single file.
      _, fns_list = modeled_data.GetSubjobsInfo()
      fn_seph_list = [fn for fns in fns_list for fn in fns]
      args=''
      for i in range(0,len(fn_seph_list)):
        args = '%s %s' % (args,fn_seph_list[i])
      cmd1 = 'Cat %s > %s' % (args,self.outputpath)
      cmd = '%s;' % (cmd1)
      if (debug):
          print('-----------------------------------------------------------------')
          print(cmd)
      subprocess.call(cmd,shell=True)
      return





if __name__ == '__main__':
  print "Run BatchTaskExecutor_test with params:", sys.argv
  # Check basic environment variable setup.
  assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
  assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
  eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
  param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)
  # f = open("modeled_data_screen_out.txt",'a+')

  #------- MAKE THE MODELED DATA ----------------
  start_time = time.time()
  modeled_data=make_MODELED_DATA(param_reader)
  modeled_data.MODELED_DATA(param_reader, True)
  elapsed_time = (time.time() - start_time)/60.0
  print("ELAPSED TIME FOR MODELED DATA (minutes) : ")
  print(elapsed_time)
