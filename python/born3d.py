#!/usr/bin/python

import time
import subprocess
import math
import copy
from batch_task_executor import *


class BORN(BatchTaskComposer):
    def __init__(self, param_reader, vel_path, prefix, adjointin, datapath, modelpath, debug, maskedshown):
        BatchTaskComposer.__init__(self)

        #Read in the job parameters
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nshots = int(self.dict_args['nfiles'])
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.prefix = prefix
        self.vel_path = vel_path
        self.adjoint = int(adjointin)
        self.datapath = datapath
        self.modelpath = modelpath
        self.singleshotdata = "%s/singleshot" % (self.path_out)
        self.eic = 0 #self.dict_args['eic']
        self.fullsrcwave_name = self.dict_args['fullsrcwave_name']
        self.fullrecwave_name = self.dict_args['fullrecwave_name']
        self.genpar = self.dict_args['genpar']
        self.readsrc = self.dict_args['readsrc']
        self.writescat = self.dict_args['writescat']

        if (int(self.eic)):
            self.angle = self.dict_args['reflangle']

        # Create all subjobs info
        self.njobs = int(math.ceil(self.nshots/float(self.nsh_perjob)))
        self.subjobids = range(0, self.njobs)
        self.subjobfns_list = [None]*self.njobs
        self.subjobIND_list = range(self.ish_beg, self.nshots+1, self.nsh_perjob) # List of actual shot number (starting @ 0)

        # Make a list of all the files for each job ( shots per job)
        for i in range(0,self.njobs):
            ish_start = self.subjobIND_list[i]
            nsh = min(self.nsh_perjob, self.nshots-ish_start)
            self.subjobfns_list[i] = [None]*nsh
            for j in range(0,nsh):
                self.subjobfns_list[i][j] = '%s/%s-%s.H' % (self.path_out,self.prefix,ish_start+j+1)
        return


    def GetSubjobScripts(self, subjobid):
        #'''Return the scripts content for that subjob.'''
        subjobfns = self.subjobfns_list[subjobid]

        # Build scripts that create the files designated in subjobfns
        scripts = []
        cnt = 0
        nf_act = len(subjobfns) # number of files

        for fn in subjobfns:
            shot = self.nsh_perjob*subjobid+cnt+1
            windowshot =  shot-1
            cnt += 1
            if cnt > nf_act: break

            operator = "%s/%s" % (self.T_BIN,'FBI.x')
            if (int(self.readsrc)==1):
                inputs = 'oper=1 par=%s  isou=%s  adj=%s wavelet=%s < %s srcwave_in=%s/%s_%s.H verbose=1' % (self.genpar,shot,self.adjoint,self.wavelet,self.vel_path,self.path_out,self.fullsrcwave_name,shot)
            else:
                inputs = 'oper=1 par=%s  isou=%s  adj=%s wavelet=%s < %s verbose=1' % (self.genpar,shot,self.adjoint,self.wavelet,self.vel_path)

            if (int(self.writescat)==1):
                scatterm = 'scat_wave_out=%s/born_scat_wave%s.H verbose=1' % (self.path_out,shot)
            else:
                scatterm = ' '


            if self.adjoint==1:
                shot_select = "Window3d f3=%s n3=1 < %s > %s_%s.H " % (windowshot,self.datapath,self.singleshotdata,shot)
                if (int(self.eic)):
                    outputs = 'data=%s_%s.H   > %s   rec_wave_out=%s/%s_%s.H  angle=%s ' % (self.singleshotdata, shot, fn, self.path_out,self.fullrecwave_name,shot,self.angle)
                else:
                    outputs = 'data=%s_%s.H   > %s   rec_wave_out=%s/%s_%s.H ' % (self.singleshotdata, shot, fn, self.path_out,self.fullrecwave_name,shot)
            else:
                shot_select = ''
                outputs = '>%s  refl=%s' % (fn, self.modelpath)
            cmd = '%s; %s %s %s %s; \n' % (shot_select, operator, inputs, scatterm, outputs) # Note: need the \n at the end... also cant use < in string itself.
            scripts.append(cmd)

        return scripts, str(subjobid)



    def GetSubjobsInfo(self):
        '''Should return two lists, subjobids and subjobfns.'''
        return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)





class make_BORN(object):
    def __init__(self, param_reader):
        dict_args = param_reader.dict_args
        self.vel_path = dict_args['vel_path']
        self.prefix = dict_args['prefix']
        self.adjoint = int(dict_args['adjoint'])
        self.datapath = dict_args['datapath']
        self.modelpath = dict_args['modelpath']
        self.debug = dict_args['debug']
        self.cleanup = dict_args['cleanup']
        self.path_out = dict_args['path_out']
        return

    def BORN_run(self, param_reader):
        #----- RUN BORN OPERATION -------------------
        bornjob = BORN(param_reader, self.vel_path, self.prefix, self.adjoint, self.datapath, self.modelpath, self.debug,'full')
        bte = BatchTaskExecutor(param_reader)
        if (self.debug):
            print('-----------------------------------------------------------------')
            print(' RUNNING BORN OPERATOR')
        bte.LaunchBatchTask(self.prefix, bornjob)
        # Group the output files
        _, fns_list = bornjob.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        args=''
        for i in range(0,len(fn_seph_list)):
            args = '%s %s' % (args,fn_seph_list[i])

        # Cat or Stack depending on the adjoint flag
        if (self.adjoint):
            cmd = 'Cat axis=4 %s | Transp plane=24 | Stack3d maxsize=10000 | Transp plane=23 > %s' % (args,self.modelpath)
        else:
            cmd = 'Cat axis=3 %s > %s' % (args,self.datapath)
        if (self.debug):
            print('-----------------------------------------------------------------')
            print(cmd)
        subprocess.call(cmd,shell=True)


        #----- Removing ADJOINT and FORWARD BORN RBF output files from wrk directory -------------
        if (int(self.cleanup)==1):
            ars_hess=''
            for i in range(0,len(fn_seph_list)):
              ars_hess = '%s %s' % (ars_hess,fn_seph_list[i])
            cmd1 = 'rm -rf %s' % (ars_hess)
            cmd2 = 'rm -rf %s/tmp*' % (self.path_out)
            cmd3 = 'rm -rf %s/singleres*' % (self.path_out)
            cmd  = '%s; %s; %s;' % (cmd1, cmd2, cmd3)
            subprocess.call(cmd,shell=True,stdout=False)
            print "--------- Removing ADJOINT and FORWARD BORNRBF output files from wrk directory  ------------------------------"
            print(cmd)

        return



if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)
    f = open("born_screen_out.txt",'a+')

    #------- MAKE THE MODELED DATA ----------------
    start_time = time.time()
    born_obj=make_BORN(param_reader)
    born_obj.BORN_run(param_reader)
    elapsed_time = (time.time() - start_time)/60.0
    print("ELAPSED TIME FOR BORN (minutes) : ")
    print(elapsed_time)
