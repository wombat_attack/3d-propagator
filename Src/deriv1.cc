
//==============================================================================================
//
//        SIMPLE 1D First derivative operator (forward)
//        Taylor Dahlke, taylor@sep.stanford.edu, 11/2/2017
//
//==============================================================================================

#include "boost/multi_array.hpp"
#include "timer.h"
#include <float1DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;


int main(int argc, char **argv){

        timer x=timer("FULL RUN");
        x.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj>  par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> wav=io->getRegFile("in",usageIn);
        std::shared_ptr<hypercube> hyperWav=wav->getHyper();
        std::shared_ptr<float1DReg> waveIN(new float1DReg(hyperWav));
        wav->readFloatStream(waveIN->getVals(),hyperWav->getN123()); 

        //==========================================================================
        // Get acquisition parameters
        float dt = hyperWav->getAxis(1).d;
        int   nt = hyperWav->getAxis(1).n;
        fprintf(stderr, "nt=%d\n",nt );
        fprintf(stderr, "dt=%f\n",dt );


        // Derivative output
        std::shared_ptr<float1DReg> waveOUT(new float1DReg(hyperWav));
        std::shared_ptr<genericRegFile> outWV=io->getRegFile("out",usageOut);
        outWV->setHyper(hyperWav);
        outWV->writeDescription();


        fprintf(stderr, "Do first derivative\n");
        for(int it=1; it < nt; it++) {
                (*waveOUT->_mat)[it] =  ((*waveIN->_mat)[it]-(*waveIN->_mat)[it-1])/dt;
        }


        // =============================================================================
        // Write the output to SEP files
        outWV->writeFloatStream(waveOUT->getVals(),hyperWav->getN123());

        x.stop();
        x.print();

}
