
//==============================================================================================
//
//        3D BORN APPROXIMATION CODE
//        Taylor Dahlke, taylor@sep.stanford.edu, 10/23/2017
//
//      This program runs the Born approximation to the acoustic wave equation. It uses 
//      TBB vectorization for the inner loop to speed things up. I follow the formulation
//      described in the paper:
//
//      "Accurate implementation of two-way wave-equation operators", Ali Almomim
//
//      Absorbing boundary conditions are simple dampening in a padding region around the model.
//
//      FORWARD OPERATOR:  
//         reflectivity space (Nz x NX x NY) ---->>  residual space (NT x NREC)
//
//      ADJOINT OPERATOR:  
//         residual space (NT x NREC) ---->>  reflectivity space (Nz x NX x NY)
//
//==============================================================================================

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacianOP3d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 3> float3D;
class block {
public:
block(int b1,int e1,int b2,int e2,int b3,int e3) : _b1(b1),_e1(e1),_b2(b2),_e2(e2),_b3(b3),_e3(e3){
};
int _b1,_e1,_b2,_e2,_b3,_e3;
};



int main(int argc, char **argv){

        timer x=timer("FULL RUN");
        x.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> velin=io->getRegFile("vel",usageIn);
        std::shared_ptr<genericRegFile> sou=io->getRegFile("sou",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<hypercube> hyperVel=velin->getHyper();
        std::shared_ptr<hypercube> hyperSou=sou->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();

        //==========================================================================
        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long nx=velN[1];
        long long ny=velN[2];
        float dz = hyperVel->getAxis(1).d;
        float dx = hyperVel->getAxis(2).d;
        float dy = hyperVel->getAxis(3).d;
        float oz = hyperVel->getAxis(1).o;
        float ox = hyperVel->getAxis(2).o;
        float oy = hyperVel->getAxis(3).o;

        //==========================================================================
        // Get acquisition parameters
        std::vector<int> souN=hyperSou->getNs();
        std::vector<int> recN=hyperRec->getNs();
        int Nsou=souN[0];
        int Nrec=recN[0];

        //==========================================================================
        // Get propagation parameters
        int nt=par->getInt("nt",100);
        float vconst=par->getFloat("vconst",2.0);
        float dsamp=par->getFloat("dsamp",0.1);
        float dt=par->getFloat("dt",0.002);
        int adj=par->getInt("adj",0);

        SEP::axis axT(nt,0.0,dt);
        SEP::axis axR(Nrec,0.0,1.0);
        SEP::axis axZ = hyperVel->getAxis(1);
        SEP::axis axX = hyperVel->getAxis(2);
        SEP::axis axY = hyperVel->getAxis(3);


        //==========================================================================
        // Model input/output
        std::shared_ptr<genericRegFile> refl;
        std::shared_ptr<float3D> model1(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3DReg> model2(new float3DReg(hyperVel));

        if(adj){// REFLECTIVITY OUTPUT
                fprintf(stderr, "REFLECTIVITY OUTPUT\n" );
                refl=io->getRegFile("out",usageOut);
                refl->setHyper(hyperVel);
                refl->writeDescription();
        }
        else{   // REFLECTIVITY INPUT
                fprintf(stderr, "REFLECTIVITY INPUT\n" );
                refl=io->getRegFile("in",usageIn);
                refl->readFloatStream(model2->getVals(),hyperVel->getN123());  
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        model1->data()[ii] = (*model2->_mat)[iy][ix][iz];
                                }
                        }
                }  
        }


        //==========================================================================
        // Data input / output
        std::shared_ptr<hypercube> hyperShotG(new hypercube(axT,axR));
        std::shared_ptr<float2DReg> data(new float2DReg(hyperShotG));
        std::shared_ptr<genericRegFile> shotG;
        if(adj){// SHOT GATHER INPUT
                fprintf(stderr, "SHOT GATHER INPUT\n" );
                shotG=io->getRegFile("in",usageIn);
                shotG->readFloatStream(data->getVals(),hyperShotG->getN123());    
        }
        else{   // SHOT GATHER OUTPUT
                fprintf(stderr, "SHOT GATHER OUTPUT\n" );
                shotG=io->getRegFile("out",usageOut);
                shotG->setHyper(hyperShotG);
                shotG->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather output \n");
        }
        //==========================================================================
        // Auxilary inputs and outputs

        // Read in the source wavelet
        fprintf(stderr, "WAVELET INPUT\n" );
        std::shared_ptr<genericRegFile> wav;
        std::shared_ptr<hypercube> hyperWav(new hypercube(axT));
        std::shared_ptr<float1DReg> wavelet(new float1DReg(hyperWav));
        wav=io->getRegFile("wavelet",usageIn);
        wav->readFloatStream(wavelet->getVals(),hyperWav->getN123()); 

        // Read in the velocity model input
        std::shared_ptr<float3DReg> velModel(new float3DReg(hyperVel));
        velin->readFloatStream(velModel->getVals(),hyperVel->getN123());

        // Read in the sou position coordinates input
        std::shared_ptr<float2DReg> soucoor(new float2DReg(hyperSou));
        sou->readFloatStream(soucoor->getVals(),hyperSou->getN123());

        // Read in the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

        // WAVEFIELD OUTPUT
        std::shared_ptr<hypercube> hyperOut3d(new hypercube(axZ,axX,axT));
        std::shared_ptr<float3DReg> wave(new float3DReg(hyperOut3d));
        std::shared_ptr<genericRegFile> outWV=io->getRegFile("wave",usageOut);
        outWV->setHyper(hyperOut3d);
        outWV->writeDescription();
        fprintf(stderr,">>>>> Done setting up the wavefield output \n");


        //==========================================================================
        // Setup the absorbing boundary condition weights input
        std::shared_ptr<float3D> abcW2(new float3D(boost::extents[ny][nx][nz]));
        int useabcs=par->getInt("useabcs",1);
        if(useabcs){
                fprintf(stderr, "Using ABC dampening \n");
                std::shared_ptr<genericRegFile> abc=io->getRegFile("abc",usageIn);
                std::shared_ptr<hypercube> hyperAbc=abc->getHyper();
                std::shared_ptr<float3DReg> abcW(new float3DReg(hyperAbc));
                abc->readFloatStream(abcW->getVals(),hyperAbc->getN123());
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        abcW2->data()[ii] = (*abcW->_mat)[iy][ix][iz];
                                }
                        }
                }
        }
        else{
                fprintf(stderr, "Using no ABC dampening \n");
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        abcW2->data()[ii] = 1.0;
                                }
                        }
                }

        }


        //==========================================================================
        // Read in the background velocity model (v0) and scale
        std::shared_ptr<float3D> v2dt(new float3D(boost::extents[ny][nx][nz]));
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                long long ii = iy*nx*nz + ix*nz + iz;
                                v2dt->data()[ii] = ((*velModel->_mat)[iy][ix][iz])*((*velModel->_mat)[iy][ix][iz])*dt*dt;
                        }
                }
        }

        // =====================================================================
        // Initialize laplacian FD coefficients and pressure fields
        float coef[6];
        float sc=1.0/(dsamp*dsamp);
        coef[1]=.83333333*sc;
        coef[2]=-.119047619*sc;
        coef[3]=.0198412698*sc;
        coef[4]=.00248015873*sc;
        coef[5]=.0001587301587*sc;
        coef[0]=-6*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);

        std::shared_ptr<float3D> oldS(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curS(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newS(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newFS(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> tmpS(new float3D(boost::extents[ny][nx][nz]));

        std::shared_ptr<float3D> oldR(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curR(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newR(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newFR(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> tmpR(new float3D(boost::extents[ny][nx][nz]));

        std::shared_ptr<float3D> oldTMP(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curTMP(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newTMP(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> temp(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> zeros(new float3D(boost::extents[ny][nx][nz]));

        for(long long i=0; i < nz*nx*ny; i++) {
                newS->data()[i]=0.0;
                newFS->data()[i]=0;
                newR->data()[i]=0;
                newFR->data()[i]=0;
                oldR->data()[i]=0;
                oldS->data()[i]=0;
                curR->data()[i]=0;
                curS->data()[i]=0;
                oldTMP->data()[i]=0;
                curTMP->data()[i]=0;
                newTMP->data()[i]=0;
                temp->data()[i]=0;
                zeros->data()[i]=1.0;
        }

        //=====================================================================
        if(adj){// READ WAVEFIELD PREVIOUS FRAME INPUT
                fprintf(stderr, "READ WAVEFIELD PREVIOUS FRAME INPUT \n");
                std::shared_ptr<float3DReg> old2(new float3DReg(hyperVel));
                std::shared_ptr<genericRegFile> inPRE;
                inPRE=io->getRegFile("curW",usageIn); // Switched for time reversal
                inPRE->readFloatStream(old2->getVals(),hyperVel->getN123());
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        oldS->data()[ii] = (*old2->_mat)[iy][ix][iz];
                                }
                        }
                }
        }
        if(adj){ // READ WAVEFIELD CURRENT FRAME INPUT
                fprintf(stderr, "READ WAVEFIELD CURRENT FRAME INPUT \n");
                std::shared_ptr<float3DReg> cur2(new float3DReg(hyperVel));
                std::shared_ptr<genericRegFile> inCUR;
                inCUR=io->getRegFile("oldW",usageIn); // Switched for time reversal
                inCUR->readFloatStream(cur2->getVals(),hyperVel->getN123());
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        curS->data()[ii] = (*cur2->_mat)[iy][ix][iz];
                                }
                        }
                }
        }

        //=====================================================================
        // Set blocksize
        int bs=par->getInt("bs",20);
        int bs1,bs2,bs3;
        bs1=bs;
        bs2=bs;
        bs3=bs;

        vector<int> b1(1,5),e1(1,5+bs1);
        int nleft=nz-10-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,5),e2(1,5+bs2);
        nleft=nx-10-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b3(1,5),e3(1,5+bs3);
        nleft=ny-10-bs3;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs3);
                b3.push_back(e3[i]);
                e3.push_back(e3[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<block> blocks;
        for(int i3=0; i3<b3.size(); ++i3) {
                for(int i2=0; i2<b2.size(); ++i2) {
                        for(int i1=0; i1<b1.size(); ++i1) {
                                blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                        }
                }
        }

        //=====================================================================
        // Specify source position
        int isou=0;
        int souz=(*soucoor->_mat)[0][isou];
        int soux=(*soucoor->_mat)[1][isou];
        int souy=(*soucoor->_mat)[2][isou];
        fprintf(stderr,"souz = %d\n",souz);
        fprintf(stderr,"souy = %d\n",souy);
        fprintf(stderr,"soux = %d\n",soux);
        float v2dti=(-1.0)*((*velModel->_mat)[souy][soux][souz])*((*velModel->_mat)[souy][soux][souz])*dt*dt;
        float dt2=dt*dt;

        //========  MAIN WAVEFIELD LOOP ========================================
        timer x2=timer("MAIN WAVEFIELD LOOP");
        x2.start();


        if (adj){ // ADJOINT   
                fprintf(stderr, "ADJOINT OPERATOR \n");

                // Kick start the source wavefield using (*wavelet->_mat)[nt-1] as the source
                for(int it=0; it < 2; it++) {
                        // Inject receiver recordings (reverse time)
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[0][irec];
                                int recx=(*reccoor->_mat)[1][irec];
                                int recy=(*reccoor->_mat)[2][irec];
                                (*newFR)[recy][recx][recz]=(*data->_mat)[irec][nt-1-it];
                        }
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[souy][soux][souz] = v2dti*(*wavelet->_mat)[nt-1];
                        // Do time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                                                        // Inject and propagate source wavefield
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                        // Adjoint stepping kernel (reverse time rec wavefield)
                                                        laplacianADJ3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                        // Scale wavefield
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,oldR->data()+i,oldTMP->data()+i,v2dt->data()+i);
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,curR->data()+i,curTMP->data()+i,v2dt->data()+i);
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,newR->data()+i,newTMP->data()+i,v2dt->data()+i);
                                                        // Take the second derivative
                                                        secder3d(blocks[ib]._e1-blocks[ib]._b1,oldTMP->data()+i,curTMP->data()+i,newTMP->data()+i,temp->data()+i,dt2);
                                                        // Imaging condition
                                                        scatADJ3d(blocks[ib]._e1-blocks[ib]._b1,newS->data()+i,temp->data()+i,model1->data()+i);
                                                }
                                        }
                                }
                        });
                        // Extract the wavefield for debugging
                        (*wave->_mat)[it] = (*model1)[souy];
                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }
                // MAIN LOOP
                for(int it=2; it < nt; it++) {
                        // Inject receiver recordings (reverse time)
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[0][irec];
                                int recx=(*reccoor->_mat)[1][irec];
                                int recy=(*reccoor->_mat)[2][irec];
                                (*newFR)[recy][recx][recz]=(*data->_mat)[irec][nt-1-it];
                        }
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[souy][soux][souz] = v2dti*(*wavelet->_mat)[nt+1-it];
                        // Do time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                                                        // Inject and propagate source wavefield
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                        // Adjoint stepping kernel (reverse time rec wavefield)
                                                        laplacianADJ3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                        // Scale wavefield
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,oldR->data()+i,oldTMP->data()+i,v2dt->data()+i);
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,curR->data()+i,curTMP->data()+i,v2dt->data()+i);
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,newR->data()+i,newTMP->data()+i,v2dt->data()+i);
                                                        // Take the second derivative
                                                        secder3d(blocks[ib]._e1-blocks[ib]._b1,oldTMP->data()+i,curTMP->data()+i,newTMP->data()+i,temp->data()+i,dt2);
                                                        // Imaging condition
                                                        scatADJ3d(blocks[ib]._e1-blocks[ib]._b1,newS->data()+i,temp->data()+i,model1->data()+i);
                                                }
                                        }
                                }
                        });
                        // Extract the wavefield for debugging
                        (*wave->_mat)[it] = (*model1)[souy];
                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }
        }
        else{ // FORWARD
                fprintf(stderr, "FORWARD OPERATOR \n");
                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[souy][soux][souz] = v2dti*(*wavelet->_mat)[it];
                        // Do time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                        // Scatter source wave timeframe against refl model
                                                        scatFOR3d(blocks[ib]._e1-blocks[ib]._b1,newS->data()+i,newTMP->data()+i,model1->data()+i);
                                                        scatFOR3d(blocks[ib]._e1-blocks[ib]._b1,curS->data()+i,curTMP->data()+i,model1->data()+i);
                                                        scatFOR3d(blocks[ib]._e1-blocks[ib]._b1,oldS->data()+i,oldTMP->data()+i,model1->data()+i);
                                                        // Take the second derivative (rec wavefield)
                                                        secder3d(blocks[ib]._e1-blocks[ib]._b1,oldTMP->data()+i,curTMP->data()+i,newTMP->data()+i,temp->data()+i,dt2);
                                                        // Scale the wavefield (rec wavefield)
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,temp->data()+i,newFR->data()+i,v2dt->data()+i);
                                                        // Propagate the scattered data (rec wavefield)
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                }
                                        }
                                }
                        });
                        // Extract reciever recordings
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[0][irec];
                                int recx=(*reccoor->_mat)[1][irec];
                                int recy=(*reccoor->_mat)[2][irec];
                                (*data->_mat)[irec][it]=(*newR)[recy][recx][recz];
                        }
                        // Extract the wavefield for debugging
                        (*wave->_mat)[it] = (*newR)[souy];
                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }
        }
        x2.stop();
        x2.print();


        // =============================================================================
        // Write the outputs to SEP files
        outWV->writeFloatStream(wave->getVals(),hyperOut3d->getN123());  // Full wavefield at souy
        if(adj){
                fprintf(stderr, "Writing out the reflection model \n");
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        (*model2->_mat)[iy][ix][iz]= model1->data()[ii];
                                }
                        }
                }  
                refl->writeFloatStream(model2->getVals(),hyperVel->getN123());  // Wavelet
        }
        else{
                fprintf(stderr, "Writing out the shot gather \n");
                shotG->writeFloatStream(data->getVals(),hyperShotG->getN123());  // Shot gather
        }

        x.stop();
        x.print();

}
