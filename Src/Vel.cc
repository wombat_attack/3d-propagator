#include <float3DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;



int main(int argc, char **argv){

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Get model parameters
        int nz=par->getInt("nz",200);
        int nx=par->getInt("nx",200);
        int ny=par->getInt("ny",1);
        float dz=par->getFloat("dz",0.001);
        float dx=par->getFloat("dx",0.001);
        float dy=par->getFloat("dy",0.001);
        float oz=par->getFloat("oz",0.0);
        float ox=par->getFloat("ox",0.0);
        float oy=par->getFloat("oy",0.0);
        float vc=par->getFloat("vc",1.5);
        float vr1=par->getFloat("vr1",2.0);
        int zr1=par->getInt("zr1",0);
        fprintf(stderr,">>>>> Done reading inputs \n");

        // Make output hypercube
        SEP::axis ax1(nz,oz,dz);
        SEP::axis ax2(nx,ox,dx);
        SEP::axis ax3(ny,oy,dy);
        std::shared_ptr<hypercube> hyperOut(new hypercube(ax1,ax2,ax3));
        std::shared_ptr<float3DReg> model(new float3DReg(hyperOut));

        // Set up the output IO stream
        std::shared_ptr<genericRegFile> outp=io->getRegFile("out",usageOut);
        outp->setHyper(hyperOut);
        outp->writeDescription();
        fprintf(stderr,">>>>> Done setting up the output stream \n");

        // Write the velocity model
        for(int k=0; k < ny; k++) {
                for(int j=0; j < nx; j++) {
                        // Top layer
                        for(int i=0; i < zr1; i++) {
                                (*model->_mat)[k][j][i] = vc;
                        }
                        // Bottom layer
                        for(int i=zr1; i < nz; i++) {
                                (*model->_mat)[k][j][i] = vr1;
                        }
                }
        }

        // Write the output to a SEP file
        outp->writeFloatStream(model->getVals(),hyperOut->getN123());

        fprintf(stderr,">>>>> Vel.x done making model \n");
}
