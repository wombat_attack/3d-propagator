extern "C" {
void laplacianFOR3d(int n, int n1, int n12, float *coef,float *v2dt,float *abc, float *prev,float *cur,float *next,float *nextF);
}

extern "C" {
void waveADJ(int n, int n1, int n12, float *coef,float *v2dt, float *abc, float *prev,float *curT, float *cur,float *next,float *nextF);
}

extern "C" {
void scatFOR3d(int n, float *wvfld,float *data, float *refl);
}

extern "C" {
void simpleMult(float *coef,float *model, float *data);
}

extern "C" {
void scatADJ3d(int n, float *wvfld,float *data, float *refl);
}

extern "C" {
void scale3d(int n, float *model,float *data, float *coef);
}

extern "C" {
void secder3d(int n, float *prev,float *curr, float *next, float *out, float dt2);
}





extern "C" {
void laplacianADJ3dOLD(int n, int n1, int n12, float *coef,float *v2dt, float *abc, float *prev,float *cur,float *next,float *nextF);
}


// extern "C" {
// void laplacianADJ3d(int n, int n1, int n12, float *coef, float *model, float *data);
// }

// extern "C" {
// void step3d(int n, float *curTemp,float *abc, float *prev, float *cur, float *next, float *nextF);
// }
