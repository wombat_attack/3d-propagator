

#include "boost/multi_array.hpp"
#include "timer.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;



int main(int argc, char **argv){

        timer x=timer("TIME TO BUILD ABC WEIGHTS:");
        x.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> inp=io->getRegFile("in",usageIn);
        std::shared_ptr<hypercube> hyperVel=inp->getHyper();

        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long nx=velN[1];
        long long ny=velN[2];
        float dz = hyperVel->getAxis(1).d;
        float dx = hyperVel->getAxis(2).d;
        float dy = hyperVel->getAxis(3).d;
        float oz = hyperVel->getAxis(1).o;
        float ox = hyperVel->getAxis(2).o;
        float oy = hyperVel->getAxis(3).o;

        // Get ABC parameters
        int bcX=par->getInt("bcX",50);
        int bcY=par->getInt("bcY",50);
        int bcZ=par->getInt("bcZ",50);
        float alpha=par->getFloat("alpha",5.0);

        // ABC DAMPENING OUTPUT
        SEP::axis axZ(nz,oz,dz);
        SEP::axis axX(nx,ox,dx);
        SEP::axis axY(ny,oy,dy);
        std::shared_ptr<hypercube> hyperOut3dABC(new hypercube(axZ,axX,axY));
        std::shared_ptr<float3DReg> abc(new float3DReg(hyperOut3dABC));
        std::shared_ptr<genericRegFile> outABC=io->getRegFile("out",usageOut);
        outABC->setHyper(hyperOut3dABC);
        outABC->writeDescription();
        fprintf(stderr,">>>>> Done setting up the ABC dampening output \n");


        //========  MAKE ABC WEIGHTS ========================================
        // Initialize array of ones
        for(long long iy=0; iy < ny; iy++){
                for(long long ix=0; ix < nx; ix++){
                        for(long long iz=0; iz < nz; iz++){
                                (*abc->_mat)[iy][ix][iz]=1.0;
                        }
                }
        }
        // Assign the exponential weights to the ABC dampening
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                // Y dampening boundaries
                                if (iy <= bcY){
                                        float yy = (iy-bcY)/(alpha*float(bcY));
                                        // (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*cos(zz*pi/2);
                                        (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*exp(-1.0*alpha*pow(yy,2));
                                }
                                else if ((ny-bcY) <= iy){
                                        float yy = (iy-ny+bcY)/(alpha*float(bcY));
                                        // (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*cos(zz*pi/2);
                                        (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*exp(-1.0*alpha*pow(yy,2));
                                }
                                // X dampening boundaries
                                if (ix <= bcX){
                                        float xx = (ix-bcX)/(alpha*float(bcX));
                                        // (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*cos(xx*pi/2);
                                        (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*exp(-1.0*alpha*pow(xx,2));
                                }
                                else if ((nx-bcX) <= ix){
                                        float xx = (ix-nx+bcX)/(alpha*float(bcX));
                                        // (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*cos(xx*pi/2);
                                        (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*exp(-1.0*alpha*pow(xx,2));
                                }
                                // Z dampening boundaries
                                if (iz <= bcZ){
                                        float zz = (iz-bcZ)/(alpha*float(bcZ));
                                        // (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*cos(zz*pi/2);
                                        (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*exp(-1.0*alpha*pow(zz,2));
                                }
                                else if ((nz-bcZ) <= iz){
                                        float zz = (iz-nz+bcZ)/(alpha*float(bcZ));
                                        // (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*cos(zz*pi/2);
                                        (*abc->_mat)[iy][ix][iz] = (*abc->_mat)[iy][ix][iz]*exp(-1.0*alpha*pow(zz,2));
                                }
                        }
                }
        }


        // =============================================================================
        // Write the outputs to SEP files
        outABC->writeFloatStream(abc->getVals(),hyperOut3dABC->getN123());  // ABC weights

        x.stop();
        x.print();

}
