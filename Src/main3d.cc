
//==============================================================================================
//
//        3D WAVE PROPAGATION CODE
//        Taylor Dahlke, taylor@sep.stanford.edu, 10/23/2017
//
//      This program solves the acoustic wave equation with constant density. It uses 
//      TBB vectorization for the inner loop to speed things up. I follow the formulation
//      described in the paper:
//
//      "Accurate implementation of two-way wave-equation operators", Ali Almomim
//
//      Absorbing boundary conditions are simple dampening in a padding region around the model.
//
//      FORWARD OPERATOR:  
//         wavelet (NT x 1) ---->>  shot gather (NT x NREC)
//
//      ADJOINT OPERATOR:  
//         shot gather (NT x NREC) ---->>  wavelet (NT x 1)
//
//==============================================================================================

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacianOP3d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
#include <math.h>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 3> float3D;

class block {
public:
block(int b1,int e1,int b2,int e2,int b3,int e3) : _b1(b1),_e1(e1),_b2(b2),_e2(e2),_b3(b3),_e3(e3){
};
int _b1,_e1,_b2,_e2,_b3,_e3;
};



int main(int argc, char **argv){

        timer x=timer("FULL RUN");
        x.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> velin=io->getRegFile("vel",usageIn);
        std::shared_ptr<genericRegFile> sou=io->getRegFile("sou",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<hypercube> hyperVel=velin->getHyper();
        std::shared_ptr<hypercube> hyperSou=sou->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();

        // Get acquisition parameters
        std::vector<int> souN=hyperSou->getNs();
        std::vector<int> recN=hyperRec->getNs();
        int Nsou=souN[0];
        int Nrec=recN[0];

        // Get propagation parameters
        int nt=par->getInt("nt",100);
        float vconst=par->getFloat("vconst",2.0);
        float dsamp=par->getFloat("dsamp",0.01);
        float dt=par->getFloat("dt",0.002);
        SEP::axis axT(nt,0.0,dt);

        //==========================================================================
        int adj=par->getInt("adj",0);
        std::shared_ptr<genericRegFile> wav;
        std::shared_ptr<hypercube> hyperWav(new hypercube(axT));
        std::shared_ptr<float1DReg> wavelet(new float1DReg(hyperWav));
        if(adj){
                fprintf(stderr, "WAVELET OUTPUT\n" );
                // WAVELET OUTPUT
                wav=io->getRegFile("out",usageOut);
                wav->setHyper(hyperWav);
                wav->writeDescription();}
        else{   // WAVELET INPUT
                fprintf(stderr, "WAVELET INPUT\n" );
                wav=io->getRegFile("in",usageIn);
                wav->readFloatStream(wavelet->getVals(),hyperWav->getN123());    
        }


        //==========================================================================
        SEP::axis axR(Nrec,0.0,1.0);
        std::shared_ptr<hypercube> hyperOut2d(new hypercube(axT,axR));
        std::shared_ptr<float2DReg> gather(new float2DReg(hyperOut2d));
        std::shared_ptr<genericRegFile> outSG;
        if(adj){// SHOT GATHER INPUT
                fprintf(stderr, "SHOT GATHER INPUT\n" );
                outSG=io->getRegFile("in",usageIn);
                outSG->readFloatStream(gather->getVals(),hyperOut2d->getN123());    
        }
        else{   // SHOT GATHER OUTPUT
                fprintf(stderr, "SHOT GATHER OUTPUT\n" );
                outSG=io->getRegFile("out",usageOut);
                outSG->setHyper(hyperOut2d);
                outSG->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather output \n");
        }
        //==========================================================================


        // Setup the velocity model input
        std::shared_ptr<float3DReg> velModel(new float3DReg(hyperVel));
        velin->readFloatStream(velModel->getVals(),hyperVel->getN123());

        // Setup the sou position coordinates input
        std::shared_ptr<float2DReg> soucoor(new float2DReg(hyperSou));
        sou->readFloatStream(soucoor->getVals(),hyperSou->getN123());

        // Setup the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long nx=velN[1];
        long long ny=velN[2];
        float dz = hyperVel->getAxis(1).d;
        float dx = hyperVel->getAxis(2).d;
        float dy = hyperVel->getAxis(3).d;
        float oz = hyperVel->getAxis(1).o;
        float ox = hyperVel->getAxis(2).o;
        float oy = hyperVel->getAxis(3).o;


        // Setup the absorbing boundary condition weights input
        std::shared_ptr<float3D> abcW2(new float3D(boost::extents[ny][nx][nz]));
        int useabcs=par->getInt("useabcs",0);
        // if(useabcs){
        //         fprintf(stderr, "Using ABC dampening \n");
        //         std::shared_ptr<genericRegFile> abc=io->getRegFile("abc",usageIn);
        //         std::shared_ptr<hypercube> hyperAbc=abc->getHyper();
        //         std::shared_ptr<float3DReg> abcW(new float3DReg(hyperAbc));
        //         abc->readFloatStream(abcW->getVals(),hyperAbc->getN123());
        //         for(int iy=0; iy < ny; iy++) {
        //                 for(int ix=0; ix < nx; ix++) {
        //                         for(int iz=0; iz < nz; iz++) {
        //                                 long long ii = iy*nx*nz + ix*nz + iz;
        //                                 abcW2->data()[ii] = (*abcW->_mat)[iy][ix][iz];
        //                         }
        //                 }
        //         }
        // }
        // else{
                fprintf(stderr, "Using no ABC dampening \n");
                for(int iy=0; iy < ny; iy++) {
                        for(int ix=0; ix < nx; ix++) {
                                for(int iz=0; iz < nz; iz++) {
                                        long long ii = iy*nx*nz + ix*nz + iz;
                                        abcW2->data()[ii] = 1.0;
                                }
                        }
                }
        // }



        // Set blocksize
        int bs=par->getInt("bs",20);
        int bs1,bs2,bs3;
        bs1=bs;
        bs2=bs;
        bs3=bs;

        // Read in the velocity model
        std::shared_ptr<float3D> v2dt(new float3D(boost::extents[ny][nx][nz]));
        float maxvel=(*velModel->_mat)[0][0][0];
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                long long ii = iy*nx*nz + ix*nz + iz;
                                v2dt->data()[ii] = ((*velModel->_mat)[iy][ix][iz])*((*velModel->_mat)[iy][ix][iz])*dt*dt;
                                if ( (*velModel->_mat)[iy][ix][iz] > maxvel){
                                        maxvel = (*velModel->_mat)[iy][ix][iz];
                                }
                        }
                }
        }
        fprintf(stderr,">>>>> Done reading velocity model \n");
        // Check the CFL condition
        float cfl=maxvel*dt/dsamp;
        float compval=sqrt(3.0)/3.0;
        if (cfl>=compval){
                fprintf(stderr,">>>>> ERROR: %f > %f \n",cfl,compval);
                fprintf(stderr,">>>>> maxvel=%f  dt=%f  dsamp=%f \n",maxvel,dt,dsamp);
                fprintf(stderr,"Doesn't pass the CFL condition. Either decrease dt or increase dsamp\n");
                exit (EXIT_FAILURE);
        }

        // WAVEFIELD OUTPUT
        SEP::axis axZ(nz,oz,dz);
        SEP::axis axX(nx,ox,dx);
        std::shared_ptr<hypercube> hyperOut3d(new hypercube(axZ,axX,axT));
        std::shared_ptr<float3DReg> wave(new float3DReg(hyperOut3d));
        std::shared_ptr<genericRegFile> outWV=io->getRegFile("wave",usageOut);
        outWV->setHyper(hyperOut3d);
        outWV->writeDescription();
        fprintf(stderr,">>>>> Done setting up the wavefield output \n");

        // WAVEFIELD PREVIOUS FRAME OUTPUT
        SEP::axis axY(ny,oy,dy);
        std::shared_ptr<hypercube> hyperOutFRAME(new hypercube(axZ,axX,axY));
        std::shared_ptr<genericRegFile> outPRE=io->getRegFile("oldW",usageOut);
        outPRE->setHyper(hyperOutFRAME);
        outPRE->writeDescription();
        fprintf(stderr,">>>>> Done setting up the previous wavefield frame output \n");

        // WAVEFIELD CURRENT FRAME OUTPUT
        std::shared_ptr<genericRegFile> outCUR=io->getRegFile("curW",usageOut);
        outCUR->setHyper(hyperOutFRAME);
        outCUR->writeDescription();
        fprintf(stderr,">>>>> Done setting up the current wavefield frame output \n");

        // =====================================================================
        // Initialize laplacian FD coefficients and pressure fields
        float coef[6];
        float sc=1.0/(dsamp*dsamp);
        coef[1]=.83333333*sc;
        coef[2]=-.119047619*sc;
        coef[3]=.0198412698*sc;
        coef[4]=.00248015873*sc;
        coef[5]=.0001587301587*sc;
        coef[0]=-6*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);

        std::shared_ptr<float3D> oldW(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curW(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newW(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newF(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> tmp(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curTemp1(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curTemp2(new float3D(boost::extents[ny][nx][nz]));

        for(long long i=0; i < nz*nx*ny; i++) {
                oldW->data()[i]=0;
                curW->data()[i]=0;
                newW->data()[i]=0;
                newF->data()[i]=0;
                tmp->data()[i]=0;
                curTemp1->data()[i]=0;
                curTemp2->data()[i]=0;
        }

        // =====================================================================
        vector<int> b1(1,5),e1(1,5+bs1);
        int nleft=nz-10-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,5),e2(1,5+bs2);
        nleft=nx-10-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b3(1,5),e3(1,5+bs3);
        nleft=ny-10-bs3;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs3);
                b3.push_back(e3[i]);
                e3.push_back(e3[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<block> blocks;
        for(int i3=0; i3<b3.size(); ++i3) {
                for(int i2=0; i2<b2.size(); ++i2) {
                        for(int i1=0; i1<b1.size(); ++i1) {
                                blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                        }
                }
        }

        // Specify source position
        int isou=0;
        int souz=(*soucoor->_mat)[0][isou];
        int soux=(*soucoor->_mat)[1][isou];
        int souy=(*soucoor->_mat)[2][isou];
        fprintf(stderr,"souz = %d\n",souz);
        fprintf(stderr,"souy = %d\n",souy);
        fprintf(stderr,"soux = %d\n",soux);


        //========  MAIN WAVEFIELD LOOP ========================================
        timer x2=timer("MAIN WAVEFIELD LOOP");
        x2.start();

        float v2dti=-1.0*((*velModel->_mat)[souy][soux][souz])*((*velModel->_mat)[souy][soux][souz])*dt*dt;



        if (adj){ // ADJOINT   
                fprintf(stderr, "ADJOINT OPERATOR \n");
                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));

                        // Inject reciever recordings (reverse time)
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[0][irec];
                                int recx=(*reccoor->_mat)[1][irec];
                                int recy=(*reccoor->_mat)[2][irec];
                                (*newF)[recy][recx][recz]=(*gather->_mat)[irec][nt-1-it];
                        }

                        
                        // OLD SLOWER WAVEFIELD GENERATOR

                        // // Do LaPlacian and time stepping
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                        //                         for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                        //                                 int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                        //                                 laplacianADJ3dOLD(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i, abcW2->data()+i, oldW->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                        //                         }
                        //                 }
                        //         }
                        // });



                        // NEW FASTER WAVEFIELD GENERATOR

                        // Scale current wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        scatFOR3d(nz,v2dt->data()+i3*nz,curTemp1->data()+i3*nz,curW->data()+i3*nz);
                                }
                        });


                        // Do LaPlacian and time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                                                        waveADJ(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i, abcW2->data()+i, oldW->data()+i,curTemp1->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                                                }
                                        }
                                }
                        });


                        // Scale and extract source (reverse time)
                        (*wavelet->_mat)[nt-1-it] = v2dti*(*newW)[souy][soux][souz];

                        // Update the wavefields
                        tmp=oldW; oldW=curW; curW=newW; newW=tmp;
                }
        }
        else{ // FORWARD
                fprintf(stderr, "FORWARD OPERATOR \n");

                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));

                        // Scale and inject source
                        (*newF)[souy][soux][souz] = v2dti*(*wavelet->_mat)[it];

                        // Do time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldW->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                                                }
                                        }
                                }
                        });

                        // Extract reciever recordings
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[0][irec];
                                int recx=(*reccoor->_mat)[1][irec];
                                int recy=(*reccoor->_mat)[2][irec];
                                (*gather->_mat)[irec][it]=(*newW)[recy][recx][recz];
                        }

                        // Extract the wavefield for debugging
                        (*wave->_mat)[it] = (*newW)[souy];

                        // Update the wavefields
                        tmp=oldW; oldW=curW; curW=newW; newW=tmp;
                }


                // // Make the extra two time frames for RTM purposes
                // for(int it=0; it < 2; it++) {

                //         // Scale and inject source
                //         (*newF)[souy][soux][souz] = v2dti*(*wavelet->_mat)[nt-1];

                //         // Do time stepping
                //         tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                //                 for(int ib=r.begin(); ib!=r.end(); ++ib) {
                //                         for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                //                                 for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                //                                         int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                //                                         laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,v2dt->data()+i,abcW2->data()+i,oldW->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                //                                 }
                //                         }
                //                 }
                //         });

                //         // Update the wavefields
                //         tmp=oldW; oldW=curW; curW=newW; newW=tmp;
                // }
        }

        x2.stop();
        x2.print();


        // =====================================================================
        // Ouput wavefield times frames to use for an RTM code 
        // (avoids the need to save the full wavefield)

        // Output "previous" wavefield
        std::shared_ptr<float3DReg> oldW2(new float3DReg(hyperOutFRAME));
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                long long ii = iy*nx*nz + ix*nz + iz;
                                (*oldW2->_mat)[iy][ix][iz] = oldW->data()[ii]; 
                        }
                }
        }
        outPRE->writeFloatStream(oldW2->getVals(),hyperOutFRAME->getN123());  // Shot gather


        // Output "current" wavefield
        std::shared_ptr<float3DReg> curW2(new float3DReg(hyperOutFRAME));
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                long long ii = iy*nx*nz + ix*nz + iz;
                                (*curW2->_mat)[iy][ix][iz] = curW->data()[ii]; 
                        }
                }
        }
        outCUR->writeFloatStream(curW2->getVals(),hyperOutFRAME->getN123());  // Shot gather


        // =============================================================================
        // Write the outputs to SEP files
        outWV->writeFloatStream(wave->getVals(),hyperOut3d->getN123());  // Full wavefield at souy
        if(adj){
                wav->writeFloatStream(wavelet->getVals(),hyperWav->getN123());  // Wavelet
        }
        else{
                outSG->writeFloatStream(gather->getVals(),hyperOut2d->getN123());  // Shot gather
        }

        x.stop();
        x.print();
}
