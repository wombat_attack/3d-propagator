

################################################################################
#		MAIN
################################################################################
main=$(shell pwd)
B=${main}/Bin
S=${main}/Src
D=${main}/Dat
R=${main}/Fig
O=${main}/Obj
P=${main}/Par
PY=${main}/python
SV=${main}/save
scratch=${main}/scratch
CXX=/usr/bin/g++
CFLAGS=-g -std=c++11 -openmp -Ofast 
CINCS=-I. -I${S} -I/opt/SEP/include -I/opt/genericIO/include -I/opt/gieeSolver/include -I/opt/hypercube/include 
CLIBS=-L/opt/SEP/lib -rdynamic -L/opt/gieeSolver/lib -lgieeBase -L/opt/genericIO/lib -lgenericCpp -L/opt/hypercube/lib -lhypercube -lsep3d -lsep -Wl,-rpath,/opt/SEP/lib
datapath = $(shell Get < .datapath parform=n datapath)
FIG=>/dev/null out=


include CompileMakefile
#include cardamom.make
# include sandbox.make
 include TestsMakefile

# Runs a make command inside the Singularity environment -----------------------------------	
singmake/%:
	singularity exec -H /data/biondo/tjdahlke/3d-propagator/  ../research-environ.img make $*


# Utility rules ----------------------------------------------------------------------------	
default: .datapath testF.h testA.h

burn:
	rm -f ${B}/*
	rm -f ${O}/*
	rm -f *.h
	rm -f *.H
	rm -rf core.*
	rm -rf scratch/*
	rm -rf *.p
	rm -rf *.v
	rm -rf *@@

.datapath:
	echo "datapath=${scratch}/" >> $@








