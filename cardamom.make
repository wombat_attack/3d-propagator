
# Run time parameters
hicut=15.0
wavelet=wavelet.H
parfile=${P}/cardamom_prop.p




#===============================================================
# Make acquisition

rec.p:
	python ${PY}/make_grid.py par=${P}/cardamom_recgrid.p outfile=$@

sou.p:
	python ${PY}/make_grid.py par=${P}/cardamom_sougrid.p outfile=$@

sou.H: ${B}/read_acq.x sou.p
	${B}/read_acq.x sou.p par=${P}/cardamom_prop.p > $@

rec.H: ${B}/read_acq.x rec.p
	${B}/read_acq.x rec.p par=${P}/cardamom_prop.p > $@

rec.H@@: rec.H
	Transp < rec.H > $@
	echo "label2='trace_number' label1='header_key'" >> $@
	echo "hdrkey1='z' hdrtype1='scalar_float' hdrfmt1='xdr_float'" >> $@
	echo "hdrkey2='x' hdrtype2='scalar_float' hdrfmt2='xdr_float'" >> $@
	echo "hdrkey3='y' hdrtype3='scalar_float' hdrfmt3='xdr_float'" >> $@

# ${wavelet}: ${P}/wavelet.p
# 	Wavelet par=$< | Scale dscale=100000000.0 | Bandpass fhi=${hicut} > $@

${wavelet}:
	Cp keepBin/wavelet.H $@


#====================================================================================
#====================================================================================
3dwindow=min3=204803 max3=224803 min2=40000 max2=60000 max1=4000
initvel3d.H:
	Window3d ${3dwindow} < ${D}/saltmodel.H > $@1
	< $@1 Interp maxsize=2000 d1out=20.0 d2out=20.0 d3out=20.0 > $@

obs_card.h: initvel3d.H sou.H rec.H@@ ${wavelet} ${B}/main3d.x abcs1.h
	${B}/main3d.x par=${parfile} sou=sou.H rec=rec.H wavelet=${wavelet} \
	wave=waveOBS.h abc=abcs1.h adj=0 vel=$< < ${wavelet} curW=junk1.h oldW=junk2.h > $@
	echo "gff=-1 hff=rec.H@@" >> $@

testwork:
	python ${PY}/workflow.py
