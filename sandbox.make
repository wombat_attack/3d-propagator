
# Run time parameters
hicut=15.0
wavelet=wavelet.H
parfile=${P}/3dprop.p

#===============================================================
# Program executions

rec.p:
	python ${PY}/make_grid.py par=${P}/rec_grid.p outfile=$@

sou.p:
	python ${PY}/make_grid.py par=${P}/sou_grid.p outfile=$@

sou.H: ${B}/read_acq.x sou.p
	${B}/read_acq.x sou.p > $@

rec.H: ${B}/read_acq.x rec.p
	${B}/read_acq.x rec.p > $@

rec.H@@: rec.H
	Transp < rec.H > $@
	echo "label2='trace_number' label1='header_key'" >> $@
	echo "hdrkey1='z' hdrtype1='scalar_float' hdrfmt1='xdr_float'" >> $@
	echo "hdrkey2='x' hdrtype2='scalar_float' hdrfmt2='xdr_float'" >> $@
	echo "hdrkey3='y' hdrtype3='scalar_float' hdrfmt3='xdr_float'" >> $@

# ${wavelet}: ${P}/wavelet.p
# 	Wavelet par=$< | Scale dscale=100000000.0 | Bandpass fhi=${hicut} > $@

${wavelet}:
	Cp keepBin/wavelet.H $@
#===============================================================
#		3D residual Born imaging test

abcs1.h: ${SV}/truevel.H ${B}/ABCweights.x 
	${B}/ABCweights.x par=${parfile} < $< > $@

randBack1.H: ${B}/randomB.x ${B}/Vel.x
	${B}/Vel.x par=${parfile} vr1=2.0 vc=2.0 > $@1
	${B}/randomB.x modelIn=$@1 par=${parfile} modelOut=$@
	rm $@1

${SV}/truevel.H: randBack1.H ${B}/Vel.x
	${B}/Vel.x par=${parfile} vc=0.0 vr1=0.2 > $@1
	Add $< $@1 > $@
	rm $@1

${SV}/initvel.H: randBack1.H ${B}/Vel.x
	${B}/Vel.x par=${parfile} vc=0.0 vr1=0.7 > $@1
	Add $< $@1 > $@
	rm $@1

# ${SV}/initvel.H: ${B}/Vel.x ${B}/randomB.x
# 	${B}/Vel.x par=${parfile} vr1=2.75 > $@1
# 	${B}/randomB.x modelIn=$@1 par=${parfile} modelOut=$@
# 	rm $@1


obs1.h: ${SV}/truevel.H sou.H rec.H@@ ${wavelet} ${B}/main3d.x abcs1.h
	${B}/main3d.x par=${parfile} sou=sou.H rec=rec.H wavelet=${wavelet} \
	wave=waveOBS.h abc=abcs1.h adj=0 vel=$< < ${wavelet} curW=junk1.h oldW=junk2.h > $@
	echo "gff=-1 hff=rec.H@@" >> $@

syn1.h: ${SV}/initvel.H sou.H rec.H@@ ${wavelet} ${B}/main3d.x abcs1.h
	${B}/main3d.x par=${parfile} sou=sou.H rec=rec.H wavelet=${wavelet} \
	wave=wave1.h useabcs=1 adj=0 vel=$< abc=abcs1.h curW=junk1.h oldW=junk2.h < ${wavelet} > $@
	echo "gff=-1 hff=rec.H@@" >> $@

curW.h oldW.h: ${SV}/initvel.H sou.H rec.H@@ ${wavelet} ${B}/main3d.x
	${B}/main3d.x par=${parfile} sou=sou.H rec=rec.H wavelet=${wavelet} \
	wave=wave2.h useabcs=0 adj=0 vel=$< curW=curW.h oldW=oldW.h < ${wavelet} > $@
	echo "gff=-1 hff=rec.H@@" >> $@

deltaM.H: ${SV}/truevel.H ${SV}/initvel.H
	Add scale=1,-1 $^ > $@

res1.h: obs1.h syn1.h
	Add scale=1,-1 $^ > $@

resWave.h:
	Add scale=1,-1 waveOBS.h wave1.h > $@

RTM.h: ${SV}/initvel.H sou.H rec.H ${wavelet} ${B}/BORN3d.x curW.h oldW.h res1.h
	${B}/BORN3d.x par=${parfile} sou=sou.H rec=rec.H vel=${SV}/initvel.H useabcs=0 \
	wavelet=${wavelet} adj=1 < res1.h \
	curW=curW.h oldW=oldW.h wave=waveBb.h > $@
	echo "hff=-1" >> $@

bornF.h: ${SV}/initvel.H sou.H rec.H ${wavelet} ${B}/BORN3d.x curW.h oldW.h deltaM.H
	${B}/BORN3d.x par=${parfile} sou=sou.H rec=rec.H vel=${SV}/initvel.H useabcs=0 \
	wavelet=${wavelet} adj=0 < deltaM.H \
	curW=curW.h oldW=oldW.h wave=waveBb.h > $@


#=====================================================
# View the receiver geometry
rec_map.v: rec.H@@
	Window3d < $^ n1=1 f1=1 > rx.h
	Window3d < $^ n1=1 f1=2 > ry.h
	Cmplx rx.h ry.h > coords.h
	Graph symbol='.' symbolsz=10 < coords.h >/dev/null out=$@
	tube $@ pixmaps=y

# Grab a single 2d line
one_lineRes%.h: res1.h
	Window_key synch=1 key1=x mink1=$(shell awk "BEGIN {print $*-0.5}") \
	maxk1=$(shell awk "BEGIN {print $*+0.5}") < $< > $@

# Grab a single 2d line
one_lineObs%.h: obs1.h
	Window_key synch=1 key1=x mink1=$(shell awk "BEGIN {print $*-0.5}") \
	maxk1=$(shell awk "BEGIN {print $*+0.5}") < $< > $@

# View a single line of receivers
view_oneRes%.v: one_lineRes%.h
	Grey < $< ${FIG}$@
	tube $@

# View a single line of receivers
view_oneObs%.v: one_lineObs%.h
	Grey < $< ${FIG}$@
	tube $@

compare_data%.v: one_lineRes%.h  one_lineObs%.h
	Cat3d $^ > $@1
	echo 'hff="-1"' >> $@1
	Grey gainpanel=a pclip=100 < $@1 ${FIG}$@
	rm $@1
	tube $@












# #====================================================================================
# #====================================================================================

# vel%.H: ${B}/Vel.x
# 	${B}/Vel.x par=${P}/$*prop.p > $@

# randomBvel%.H: vel%.H ${B}/randomB.x
# 	${B}/randomB.x modelIn=$< par=${P}/$*prop.p modelOut=$@

# abcs%.h: randomBvel%.H ${B}/ABCweights.x 
# 	${B}/ABCweights.x par=${P}/$*prop.p < $< > $@

# run%.h: randomBvel%.H sou.H rec.H ${wavelet} ${B}/main%.x abcs%.h
# 	${B}/main$*.x par=${P}/$*prop.p sou=sou.H rec=rec.H wavelet=${wavelet} wave=wave.h abc=abcs$*.h < $< > $@

# #=====================================================
# testF.h: vel3d.H sou.H rec.H ${wavelet} ${B}/main3d.x
# 	${B}/main3d.x par=${parfile} sou=sou.H rec=rec.H wavelet=${wavelet} \
# 	wave=wave2.h curW=curW.h oldW=oldW.h useabcs=0 adj=0 vel=$< < ${wavelet} > $@

# testA.h: vel3d.H sou.H rec.H ${wavelet} ${B}/main3d.x testF.h
# 	${B}/main3d.x par=${parfile} sou=sou.H rec=rec.H wavelet=${wavelet} \
# 	wave=wave2.h curW=curW.h oldW=oldW.h useabcs=0 adj=1 vel=$< < testF.h > $@

# #=====================================================
# # Absorbing boundary condition tests

# withABCs.h: ${SV}/initvel.H sou.H rec.H ${wavelet} ${B}/main3d.x abcs1.h
# 	${B}/main3d.x ${common} useabcs=1 adj=0 vel=$< < ${wavelet} \
# 	wave=wave1.h abc=abcs1.h  > $@

# withoutABCs.h: ${SV}/initvel.H sou.H rec.H ${wavelet} ${B}/main3d.x
# 	${B}/main3d.x ${common} useabcs=0 adj=0 vel=$< < ${wavelet} \
# 	wave=wave2.h curW=curW.h oldW=oldW.h > $@

# compareABCs: withABCs.h withoutABCs.h
# 	Cat3d $^ | Grey gainpanel=a | Tube pixmaps=y