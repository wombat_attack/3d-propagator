
# 3D WAVE PROPAGATION CODE #

This program solves the acoustic wave equation with constant density. It uses 
TBB vectorization for the inner loop to speed things up. I follow the formulation
described in the paper:

"Accurate implementation of two-way wave-equation operators", Ali Almomim

Absorbing boundary conditions are simple dampening in a padding region around the model.

#### FORWARD OPERATOR:####  
* wavelet (NT x 1) ---->>  shot gather (NT x NREC)

#### ADJOINT OPERATOR:####
* shot gather (NT x NREC) ---->>  wavelet (NT x 1)

### Who do I talk to? ###

* Taylor Dahlke
* taylor@sep.stanford.edu
* 10/23/2017
